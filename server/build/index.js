"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _helmet = _interopRequireDefault(require("helmet"));

var _passport = _interopRequireDefault(require("passport"));

var _google = _interopRequireDefault(require("./config/google.config"));

var _route = _interopRequireDefault(require("./config/route.config"));

var _index = _interopRequireDefault(require("./API/Auth/index"));

var _index2 = _interopRequireDefault(require("./API/Restaurant/index"));

var _index3 = _interopRequireDefault(require("./API/Food/index"));

var _index4 = _interopRequireDefault(require("./API/Menu/index"));

var _index5 = _interopRequireDefault(require("./API/Image/index"));

var _index6 = _interopRequireDefault(require("./API/Orders/index"));

var _index7 = _interopRequireDefault(require("./API/Reviews/index"));

var _index8 = _interopRequireDefault(require("./API/User/index"));

var _index9 = _interopRequireDefault(require("./API/Mail/index"));

var _Payments = _interopRequireDefault(require("./API/Payments"));

var _connection = _interopRequireDefault(require("./database/connection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require("dotenv").config();

var zomato = (0, _express["default"])(); //passport config

(0, _google["default"])(_passport["default"]);
(0, _route["default"])(_passport["default"]);
zomato.use(_express["default"].json());
zomato.use(_express["default"].urlencoded({
  extended: false
}));
zomato.use((0, _cors["default"])());
zomato.use((0, _helmet["default"])());
zomato.use(_passport["default"].initialize());
zomato.use(_passport["default"].session());
zomato.get("/", function (req, res) {
  res.json({
    message: "Setup Success"
  });
});
zomato.use("/auth", _index["default"]);
zomato.use("/restaurant", _index2["default"]);
zomato.use("/food", _index3["default"]);
zomato.use("/menu", _index4["default"]);
zomato.use("/image", _index5["default"]);
zomato.use("/order", _index6["default"]);
zomato.use("/review", _index7["default"]);
zomato.use("/user", _index8["default"]);
zomato.use("/mail", _index9["default"]);
zomato.use("/payments", _Payments["default"]);
zomato.listen(4000, function () {
  return (0, _connection["default"])().then(function () {
    return console.log("Server is up and running");
  })["catch"](function (error) {
    console.log(error);
    console.log("Server is running, but database connection failed ...");
  });
});